﻿using AtomConnect.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtomConnect.Core.ViewModel
{
    public class MainWindowViewModel
    {
        private Customer _customer;

        public MainWindowViewModel()
        {
        }

        public MainWindowViewModel(Customer customer)
        {
            _customer = customer;
        }
    }
}
