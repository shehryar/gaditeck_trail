﻿using Atom.Core.Models;
using Atom.SDK.Core.Models;
using Atom.SDK.Net;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AtomConnect
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string usersName = "";
        public MainWindow()
        {
            InitializeComponent();
            InitializeSDK();

            this.Title = "Atom Connect";
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }

        AtomManager atomManagerInstance;



        private async void InitializeSDK()
        {
            await Task.Factory.StartNew(() =>
            {
                var atomConfiguration = new AtomConfiguration("2a525ded0421ddbb6e166344bd514c1b");
                atomConfiguration.VpnInterfaceName = "Atom";
                atomManagerInstance = AtomManager.Initialize(atomConfiguration);

                atomManagerInstance.Connected += AtomManagerInstance_Connected;
            });
        }
        private void AtomManagerInstance_Connected(object sender, EventArgs e)
        {
            var result = atomManagerInstance.VPNConnectionState;
        }

        private void Send_Button_Click(object sender, RoutedEventArgs e)
        {
            usersName = UsersName.Text;

            atomManagerInstance.Credentials = new Credentials("purevpn0d4037095", "pvpn12345");

            var vpnProperties = new VPNProperties("2a525ded0421ddbb6e166344bd514c1b");
            atomManagerInstance.Connect(vpnProperties);

            MessageBox.Show($"Hello {usersName}");
        }

    }
}
